#!/bin/sh

# if duplicity not installed then bail out
if ! which duplicity > /dev/null ; then
  echo "duplicity not installed! exiting"
  exit 2
fi

# if specified target not a mounted location then bail out
if ! mountpoint "$1" ; then
  echo "backup target NOT mounted! exiting"
  exit 1
fi

# clear screen for run
clear

# number of full duplicity backups to keep before autoremoval
DUPFULL=3
# log dir + create as needed
LOGDIR=/var/log/userbackups
[ -d "$LOGDIR" ] || mkdir "$LOGDIR"
# script run time for log file name
CURTIME=$(date +%Y%d%m.%H.%M)

mk_bu_dir() {
  if [ ! -d "$2" ] ; then
    mkdir -p "$2"
    chown "$1":"$1" "$2"
  fi
}

usr_bu_dup() {
  echo "backing up user ${1}"
  echo ""
  mk_bu_dir "$1" "$2"
  su -c "duplicity \
    --full-if-older-than 30D \
    --no-encryption \
    --volsize=512 \
    --exclude-if-present .nobackup \
    --exclude=/home/${1}/Downloads \
    --exclude=/home/${1}/Videos \
    --exclude=/home/${1}/Music \
    --exclude=/home/${1}/.local/share/containers \
    /home/${1} \
    file://${2}" "$1"

  echo ""
  echo "cleaning old backups"
  echo ""

    # stage two clean duplicity backup
  su -c "duplicity \
    remove-all-but-n-full ${DUPFULL} \
    --force \
    file://${2}" "$1"

   echo ""
   echo "user ${1} backups complete"
   echo ""
}

for user in /home/* ; do
  UNAME=$(basename "$user")
  if [ "$UNAME" = "root" ] || \
     [ "$UNAME" = "partimg" ] || \
     [ "$UNAME" = "lost+found" ] ; then
    continue
  else
    usr_bu_dup "$UNAME" "$1"/DUPDATA/"$UNAME" >> "$LOGDIR"/"$CURTIME".backup.log
  fi
done

# clean up old backup logs
find "$LOGDIR"/ -type f -mtime +5 -exec rm {} \;
